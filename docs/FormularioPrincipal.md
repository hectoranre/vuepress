Formulario principal.
=

Menú principal.
-

Al ejecutar la aplicación se nos abrirá la ventana principal "Formulario Reservas".
En esta página tenemos dos botones: "Reservas" y "Salir". El botón "Reservas" nos llevará a la siguiente pantalla, con el formulario de Reservas a cumplimentar por el usuario. El botón "Salir" cerrará el programa en ejecución.
De forma análoga, podremos acceder a las mismas funcionalidades a través del menú superior.

Barra de menú superior.
-
Esta barra de menú consta de tres botones: "Reservas", "Salir" y "Help".
Activando el botón "Reservas", se nos dará la opción de ir directamente al formulario de reservas mientras que el botón "Salir" nos activa la opción de "Cerrar programa" y finalizar la ejecución del mismo.
El botón "Help" nos permite acceder a la ayuda de la aplicación.

