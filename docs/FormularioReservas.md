Formulario Reservas.
==
Una vez dentro del formulario, deberemos rellenar los campos necesarios para efectuar la reserva del cliente.
En la parte superior, apartado "datos del cliente", tenemos que escribir el nombre y apellidos del cliente, así como su teléfono y su e-mail, si tuviera, en los campos correspondientes.
Justo debajo, en el apartado "datos de la reserva" especificaremos las particularidades de la reserva.

Tipo de evento.
-
Aquí marcaremos si el evento es una reunión de empresa, un cumpleaños o un evento religioso que puede ser una boda, una comunión o un bautizo. Sólo se podrá marcar una de las opciones.

Asistentes al evento.
-
Este campo se deberá cumplimentar obligatoriamente, apuntando cuántas personas asistirán al evento.

Fecha del evento.
-
Aquí rellenamos el campo con la fecha de comienzo del evento. Pulsando en el icono de la derecha podremos desplegar un calendario desde donde podemos marcar el día exacto en lugar de escribirlo.

Número de habitaciones.
-
Este campo sólo estará disponible si anteriormente se ha escogido como evento un evento religioso (boda, comunión o bautizo). Hay que especificar cuántas habitaciones quiere reservar el cliente para su evento.

Número de jornadas.
-
Esta opción sólo estará disponible para eventos de tipo Reunión de empresa y deberemos escribir el número de jornadas que durará el evento.

Tipo de cocina.
-
Para cualquier tipo de evento, habrá que escoger el tipo de cocina que desea el cliente entre las opciones disponibles:
- Menú cerrado.
- A la carta.
- Lo ponen los clientes.
- Por concretar.

Generar reserva.
-
Por último, tras repasar los datos con el cliente, pulsaremos en "Aceptar" si todo está correcto o "Cancelar" en caso de que el cliente cambie de opinión.
En cualquier momento podemos cambiar las opciones y los datos introducidos hasta pulsar "Aceptar" o "Cancelar".