module.exports = {
    title: 'Gestión Hotelera App',
    description: 'Como realizar la gestión de las reservas',
    base: '/vuepress/',
    dest: 'public',

themeConfig: {
nav: [
{ text: 'Inicio', link: '/' },
{ text: 'Formulario principal', link: '/FormularioPrincipal' },
{ text: 'Formulario de reservas', link: '/FormularioReservas'},
{ text: 'Menú Tipo de cocina', link: '/TipoCocina' }
],
sidebar: [
['/', 'Inicio'],
['/FormularioPrincipal', 'Formulario principal'],
['/FormularioReservas', 'Formulario de reservas'],
['/TipoCocina', 'Menú Tipo de cocina']
]
}
}
