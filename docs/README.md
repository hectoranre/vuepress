---
home: true
heroImage: /th2.jpg
tagline: Como configurar las reservas de eventos
actionText: Accede al Manual de usuario
actionLink: ./FormularioPrincipal.md
features:
- title: Manual de usuario.
  details: Gestión Hotelera App.
- title: Gestión de clientes.
  details: Gestiona eficientemente tus reservas.
footer: MIT Licensed | Copyright © 2018-present Evan You
---