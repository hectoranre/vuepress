Menú "Tipo de cocina".
=

En este menú se puede escoger el tipo de cocina entre las cuatro opciones disponibles.

Menú cerrado.
- 
- Menú cerrado: escogeremos menú cerrado cuando el cliente desee contratar un menú específico para todos los comensales.

A la carta.
-
- A la carta: esta opción permite que los asistentes al evento puedan escoger personal e independientemente sus platos.

Lo ponen los clientes.
-

- Lo ponen los clientes: en este caso, no se servirá comida, sino que los propios clientes serán los encargados de traerla, bien ellos mismos o bien mediante un servicio de catering externo.

Por concretar.
-

- Por concretar: podemos dar la opción a los clientes de tomar la decisión sobre el tipo de cocina posteriormente a la reserva del evento.
